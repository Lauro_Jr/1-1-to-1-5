<?php

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['num1'],$_POST['num2'])) {
      $num1 = $_POST['num1'];
      $num2 = $_POST['num2'];
      $sum = $num1 + $num2;
      $diff = $num1 - $num2;
      $product = $num1 * $num2;
      $div = $num1 / $num2;
    } else {
        echo "Please enter the numbers";
    }
  }  
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <h5>The four basic operations of arithmetic.</h5>
  <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
    Enter Num1 : <input name="num1" type="number" required><br>
    Enter Num2 : <input name="num2" type="number" required><br>
    <input type="submit">
  </form>
  <?php 
   if (isset($num1) || isset($num2)) {
  ?>
  <p><?php  echo $num1.' + '.$num2. ' = '.$sum;  ?></p>
  <p><?php  echo $num1.' - '.$num2. ' = '.$diff  ?></p>
  <p><?php  echo $num1.' * '.$num2. ' = '.$product;  ?></p>
  <p><?php  echo $num1.' / '.$num2. ' = '.$div;  ?></p>
  <?php 
   }
  ?>
</body>
</html>